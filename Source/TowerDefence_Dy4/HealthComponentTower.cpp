// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponentTower.h"
#include "UiManager.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


// Sets default values
AHealthComponentTower::AHealthComponentTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHealthComponentTower::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACoreTower::StaticClass(), existingCoreTower);

	if (ACoreTower* coreActor = Cast<ACoreTower>(existingCoreTower[0]))
	{
		coreActor->OnDamaged.AddDynamic(this, &AHealthComponentTower::DecreaseHealthTower);
	}

	if (ACoreTower* coreActor = Cast<ACoreTower>(existingCoreTower[1]))
	{
		coreActor->OnDamaged.AddDynamic(this, &AHealthComponentTower::DecreaseHealthTower);
	}

	currentHealth = fullHealth;
	currentHealthPercentage = float(currentHealth) / float(fullHealth);

	PassToUiManager(currentHealthPercentage, false);
	
}

// Called every frame
void AHealthComponentTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AHealthComponentTower::PassToUiManager(float data, bool isGameOVer)
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AUiManager::StaticClass(), uiManager);

	AUiManager* uiActor = Cast<AUiManager>(uiManager[0]);

	uiActor->coreHealth = data;
	uiActor->isGameOver = isGameOVer;
}

void AHealthComponentTower::DecreaseHealthTower(float damage)
{
	//UE_LOG(LogTemp, Warning, TEXT("Current Health: , %f"), currentHealth);

	currentHealth -= damage;
	currentHealthPercentage = float(currentHealth) / float(fullHealth);

	PassToUiManager(currentHealthPercentage, false);

	if (currentHealth <= 0.0f)
	{
		PassToUiManager(currentHealthPercentage, true);
		onTowerDeath.Broadcast();
	}

}


