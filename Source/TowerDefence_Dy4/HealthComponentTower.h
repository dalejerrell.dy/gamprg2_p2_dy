// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoreTower.h"
#include "HealthComponentTower.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCoreTowerDeath);

UCLASS()
class TOWERDEFENCE_DY4_API AHealthComponentTower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHealthComponentTower();

	UPROPERTY(BlueprintAssignable, Category = "Event Dispatcher")
		FOnCoreTowerDeath onTowerDeath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float fullHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float currentHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float currentHealthPercentage;

	TArray<AActor*> existingCoreTower;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float damageReceived;

private:

	UPROPERTY()
		TArray<AActor*> uiManager;

	UFUNCTION()
		void PassToUiManager(float data, bool isGameOver);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


protected:

	UFUNCTION(BlueprintCallable)
		void DecreaseHealthTower(float damaged);


};
