// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "TimerManager.h"
#include "Enemy.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	staticMesh->SetupAttachment(RootComponent);

	collisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Range Sphere"));
	collisionSphere->SetupAttachment(staticMesh);
	collisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ABullet::OnOverlap);

	speed = 1000.0f;
	explodeDelayTime = 2.0f;
	burnEffect = 3.0f;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(bulletTimerHandler, this, &ABullet::Explode, explodeDelayTime);
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MoveBullet(DeltaTime);

}

void ABullet::OnOverlap(UPrimitiveComponent * OverlappedComp, AActor * otherActor, UPrimitiveComponent * OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Hit something"));

	BulletType(otherActor);
}

void ABullet::UpdateStats(bool splash, bool burn, int damage)
{
	isSplash = splash;
	isBurn = burn;
	bulletDamage = damage;

	if (isSplash)
	{
		collisionSphere->SetSphereRadius(500.0f);
	}
}

void ABullet::BulletType(AActor* otherActor)
{
	if (AEnemy* enemy = Cast<AEnemy>(otherActor))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Hit enemy"));

		if (isSplash)
		{
			// Missile Bullet
			//UE_LOG(LogTemp, Warning, TEXT("Determining targets"));

			TArray<AActor*> enemyActor;
			enemyActor.Add(otherActor);

			currEnemy = enemyActor[0];

			UE_LOG(LogTemp, Warning, TEXT("Current Enemy Count: %d"), enemyActor.Num());

			for (int i = 0; i < enemyActor.Num(); i++)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Exploded"));
				AEnemy* enemyRef = Cast<AEnemy>(enemyActor[i]);
				enemyRef->Attacked(bulletDamage);
			}

			GetWorldTimerManager().ClearTimer(bulletTimerHandler);
			Explode();

		}
		else
		{
			if (isBurn)
			{
				// Burn Effect
				//UE_LOG(LogTemp, Warning, TEXT("Apply burn effect to target"))
				enemy->damageOverTime(bulletDamage, burnEffect);
				GetWorldTimerManager().ClearTimer(bulletTimerHandler);
				Explode();
			}
			else
			{
				// Basic Bullet
				if (enemy->isGround)
				{
					enemy->Attacked(bulletDamage);
				}
				GetWorldTimerManager().ClearTimer(bulletTimerHandler);
				Explode();
			}
		}

	}
	
}

void ABullet::MoveBullet(float deltaTime)
{
	FVector loc = GetActorLocation();
	currentSpeed = deltaTime * speed;

	if (!isSplash)
	{
		loc += (currentSpeed)* GetTransform().GetUnitAxis(EAxis::X);
	}
	else
	{
		if (currEnemy != NULL)
		{
			loc += (currentSpeed)* currEnemy->GetActorLocation() * GetTransform().GetUnitAxis(EAxis::X);
		}
		else
		{
			loc += (currentSpeed)* GetTransform().GetUnitAxis(EAxis::X);
		}
	}

	SetActorLocation(loc);
}

void ABullet::Explode()
{
	Destroy();
}


