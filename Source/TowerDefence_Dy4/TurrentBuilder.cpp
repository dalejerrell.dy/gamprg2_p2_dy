// Fill out your copyright notice in the Description page of Project Settings.


#include "TurrentBuilder.h"
#include "UiManager.h"
#include "PlayerPawn.h"
#include "Turrent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
ATurrentBuilder::ATurrentBuilder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	staticMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATurrentBuilder::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AUiManager::StaticClass(), uiManager);
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerPawn::StaticClass(), playerRef);

	AUiManager* uiActor = Cast<AUiManager>(uiManager[0]);

	uiActor->onClickedbuild.AddDynamic(this, &ATurrentBuilder::buildTurrent);
}

// Called every frame
void ATurrentBuilder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATurrentBuilder::PassToUiManager(bool isBuilding)
{
	AUiManager* uiActor = Cast<AUiManager>(uiManager[0]);

	uiActor->isBuilding = isBuilding;
	uiActor->builderID = GetTurrentBuilderOrder();
}

void ATurrentBuilder::buildTurrent(int turrentNum, int buildChecker)
{
	turrentNum--;

	//UE_LOG(LogTemp, Warning, TEXT("I am getting %d"), turrentNum);

	if (buildChecker == GetTurrentBuilderOrder())
	{
		UWorld* const world = GetWorld();

		APlayerPawn* playerActor = Cast<APlayerPawn>(playerRef[0]);

		AUiManager* uiActor = Cast<AUiManager>(uiManager[0]);

		FTurrentConfig turrentConfig = turrent[turrentNum];

		playerActor->turretCost = turrentConfig.turrentCost;

		if (playerActor->IsBuildAffordable())
		{
			//UE_LOG(LogTemp, Warning, TEXT("I am trying to spend money"));
			uiActor->canBuy = true;

			playerActor->SpendTurretMoney();

			if (turrentConfig.turrentType)
			{
				if (world)
				{
					FActorSpawnParameters SpawnParams;
					SpawnParams.Owner = this;

					FVector SpawnLoc = this->staticMesh->GetComponentLocation() + FVector(0, 0, 60);
					FRotator SpawnRot = FRotator::ZeroRotator;

					world->SpawnActor<ATurrent>(turrentConfig.turrentType, SpawnLoc, SpawnRot, SpawnParams)->UpdateStats(turrentConfig.turrentCost);
					//UE_LOG(LogTemp, Warning, TEXT("I have spawned a tower"));

				}
			}
		}
		else
		{
			uiActor->canBuy = false;
		}
	}
	
}

int ATurrentBuilder::GetTurrentBuilderOrder()
{
	return builderOrder;
}
