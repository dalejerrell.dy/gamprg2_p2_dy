// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "Enemy.h"
#include "PlayerPawn.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_DY4_API APlayerPawn : public ASpectatorPawn
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;

	UFUNCTION()
		bool IsBuildAffordable();

	UFUNCTION()
		void SpendTurretMoney();

	UFUNCTION()
		void GainMoney(int killGain);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Economy", meta = (AllowPrivateAccess = "true"))
		int turretCost;


private:

	UPROPERTY()
		TArray<AActor*> uiManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Economy", meta = (AllowPrivateAccess = "true"))
		int currentMoney;

	UFUNCTION()
		void PassToUiManager(int data);

};
