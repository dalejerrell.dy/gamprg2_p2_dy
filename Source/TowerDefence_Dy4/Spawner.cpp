// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Enemy.h"
#include "TimerManager.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	spawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnBox"));
	RootComponent = spawnBox;

	waveIncrease = 0.0f; // added in the GameMode

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::SpawnEnemy()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Should be spawning"));

	enemyPicker = FMath::RandRange(0, enemyType.Num() - 1);

	if (enemyType[enemyPicker]) 
	{
		UWorld* const world = GetWorld();

		if (world) {
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;

			FVector SpawnLoc = UKismetMathLibrary::RandomPointInBoundingBox(spawnBox->GetComponentLocation(), spawnBox->GetScaledBoxExtent());
			FRotator SpawnRot = FRotator::ZeroRotator;

			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Have spawned already"));
			world->SpawnActor<AEnemy>(enemyType[enemyPicker], SpawnLoc, SpawnRot, SpawnParams)->UpdateStats(waveIncrease); 
			enemySpawned++;

			SpawnDelay();
		}
	}
}

void ASpawner::SpawnDelay()
{
	if (enemySpawned < (numEnemyToSpawn))
	{
		GetWorldTimerManager().SetTimer(spawnerTimerHandle, this, &ASpawner::SpawnEnemy, enemySpawnDelay, false);
	}
}


