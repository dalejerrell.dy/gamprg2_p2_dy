// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bullet.h"
#include "Turrent.generated.h"

UCLASS()
class TOWERDEFENCE_DY4_API ATurrent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATurrent();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		FString turrentName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		bool canShoot;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		bool canShootAir;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		bool isSplashDamage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		bool isGoldDigger;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		bool isHelper;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Bullet Conditions")
		int bulletDamage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Bullet Conditions")
		bool isBurnEffect;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		float fireRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		float multiplier;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		class AEnemy* currentEnemy;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Bullet Conditions")
		TSubclassOf<ABullet> projectile;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		int moneyToGive;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		float giveMoneyDelay;

	UFUNCTION()
		void UpdateStats(int turrentCost);

	UFUNCTION(BlueprintCallable)
		void SellTurrent();

	UFUNCTION(BlueprintCallable)
		void UpgradeTurrent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* staticMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* muzzleStaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* rangeSphere;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* arrow;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turrent Conditions")
		FTimerHandle turrentTimerHandle;


	UFUNCTION()
		virtual void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* otherActor, UPrimitiveComponent* OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* otherActor, UPrimitiveComponent* OtherComp, int32 otherBodyIndex);


private:	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turrent Conditions", meta = (AllowPrivateAccess = "true"))
		int cost;
	UPROPERTY()
		TArray<AActor*> playerPref;
	UPROPERTY(VisibleAnywhere)
		TArray<AActor*> targetRefs;
	UPROPERTY()
		int enemiesAffected;

	UFUNCTION()
		void DetermineTargets(AActor* otherActor);

	UFUNCTION()
		void DetermineDistanceToEnemy();

	UFUNCTION()
		void DetermineTurrentType();

	UFUNCTION()
		void Shoot();

	UFUNCTION()
		void fireRateDelay();

	UFUNCTION()
		void GainGold();

};
