// Fill out your copyright notice in the Description page of Project Settings.


#include "UiManager.h"

// Sets default values
AUiManager::AUiManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AUiManager::BeginPlay()
{
	Super::BeginPlay();
	canBuy = true;
}

// Called every frame
void AUiManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	

}

void AUiManager::BroadcastBuild(int buildTurrent)
{
	UE_LOG(LogTemp, Warning, TEXT("I am broadcasting this number %d"), buildTurrent);


	onClickedbuild.Broadcast(buildTurrent, builderID);
}

