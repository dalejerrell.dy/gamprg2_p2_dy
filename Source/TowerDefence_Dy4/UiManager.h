// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UiManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnClickedBuildSignature, int, turrentToBuild, int, builderNumber);

UCLASS()
class TOWERDEFENCE_DY4_API AUiManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUiManager();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		float coreHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		int coins;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		float timeLeftWithWave;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		int currentWave;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		int finalWave;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		bool canBuy;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		bool isWin;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI Elements", meta = (AllowPrivateAccess = "true"))
		bool isGameOver;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building Condition", meta = (AllowPrivateAccess = "true"))
		bool isBuilding;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Building Condition", meta = (AllowPrivateAccess = "true"))
		int builderID;

	UPROPERTY(BlueprintAssignable, Category = "Building Condition")
		FOnClickedBuildSignature onClickedbuild;

	UFUNCTION(BlueprintCallable)
		void BroadcastBuild(int buildTurrent);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:


};
