// Fill out your copyright notice in the Description page of Project Settings.


#include "CoreTower.h"
#include "Enemy.h"
#include "Components/BoxComponent.h"
#include "TowerDefence_Dy4GameMode.h"
#include "Engine.h"

// Sets default values
ACoreTower::ACoreTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	collisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	collisionBox->SetSimulatePhysics(true);
	collisionBox->SetNotifyRigidBodyCollision(true);
	collisionBox->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	collisionBox->OnComponentHit.AddDynamic(this, &ACoreTower::OnBoxHit);
	RootComponent = collisionBox;

}

void ACoreTower::OnBoxHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	if (AEnemy* enemy = Cast<AEnemy>(OtherActor))
	{
		damageReceived = enemy->Attack();

		OnDamaged.Broadcast(damageReceived);
	}
}

// Called when the game starts or when spawned
void ACoreTower::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHealthComponentTower::StaticClass(), existingHealth);

	if (AHealthComponentTower* healthComp = Cast<AHealthComponentTower>(existingHealth[0]))
	{
		healthComp->onTowerDeath.AddDynamic(this, &ACoreTower::DestroyTower);
	}

}

void ACoreTower::DestroyTower()
{
	Destroy();
}

// Called every frame
void ACoreTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

