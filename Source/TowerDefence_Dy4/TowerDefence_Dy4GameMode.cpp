// Copyright Epic Games, Inc. All Rights Reserved.

#include "TowerDefence_Dy4GameMode.h"
#include "Enemy.h"
#include "Spawner.h"
#include "CoreTower.h"
#include "UiManager.h"
#include "PlayerPawn.h"
#include "Timermanager.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

ATowerDefence_Dy4GameMode::ATowerDefence_Dy4GameMode()
{

}

void ATowerDefence_Dy4GameMode::BeginPlay()
{
	Super::BeginPlay();

	if (!isDebugging)
	{
		Starter();
	}
}

void ATowerDefence_Dy4GameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATowerDefence_Dy4GameMode::Starter()
{
	GetWorldTimerManager().SetTimer(waveDurationHandle, this, &ATowerDefence_Dy4GameMode::WaveCalculations, 1.0f, true);

	PassToUiManager(maxWave, currentWaveCycle, waveDuration, maxWaveDuration, false);

	AssignStats();
}

void ATowerDefence_Dy4GameMode::AssignStats()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawner::StaticClass(), spawners);

	FWaveConfig config = waveConfig[currentWaveCycle];

	//UE_LOG(LogTemp, Warning, TEXT("Wave Cycle Count: %d and Max Wave Cycle Count: %d"), currentWaveCycle, maxWave);

	for (int i = 0; i <= spawners.Num() - 1; i++)
	{
		ASpawner* spawner = Cast<ASpawner>(spawners[i]);
		spawner->enemyType = config.enemyTypes;
		spawner->numEnemyToSpawn = config.numToSpawn;
		spawner->waveIncrease += 1.0f;
		spawner->SpawnEnemy();
		maxWaveDuration = config.duration;
	}

	waveDuration = maxWaveDuration;
}

void ATowerDefence_Dy4GameMode::WaveCalculations()
{
	if (isWaveDone())
	{
		currentWaveCycle++;

		if (currentWaveCycle > maxWave)
		{

			UE_LOG(LogTemp, Warning, TEXT("Max Waves Reached: %d"), maxWave);

			if (waveDuration <= 0)
			{
				PassToUiManager(maxWave, currentWaveCycle, waveDuration, maxWaveDuration, true);
			}

			return;
		}
		else
		{
			PassToUiManager(maxWave, currentWaveCycle, waveDuration, maxWaveDuration, false);

			GiveOutBonusMoney();

			Starter();
		}
	}
	else
	{
		PassToUiManager(maxWave, currentWaveCycle, waveDuration, maxWaveDuration, false);

		waveDuration--;
	}
}

bool ATowerDefence_Dy4GameMode::isWaveDone()
{
	if (waveDuration <= 0) return true;
	else return false;
}

void ATowerDefence_Dy4GameMode::GiveOutBonusMoney()
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemy::StaticClass(), remainingEnemies);

	if (remainingEnemies.Num() == 0)
	{
		bonusMoney = bonusMoney * currentWaveCycle;

		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerPawn::StaticClass(), playerPawn);

		APlayerPawn* playerActor = Cast<APlayerPawn>(playerPawn[0]);

		playerActor->GainMoney(bonusMoney);
	}
}

void ATowerDefence_Dy4GameMode::PassToUiManager(int lastWave, int currentWave, float waveTime, float maxWaveTime, bool isWin)
{
	lastWave = maxWave;
	currentWave = currentWaveCycle;
	waveTime = waveDuration;
	maxWaveTime = maxWaveDuration;

	waveTime = waveTime / maxWaveTime;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AUiManager::StaticClass(), uiManager);

	AUiManager* uiActor = Cast<AUiManager>(uiManager[0]);

	uiActor->timeLeftWithWave = waveTime;
	uiActor->currentWave = currentWave + 1;
	uiActor->finalWave = lastWave;
	uiActor->isWin = isWin;
	uiActor->isGameOver = isWin;

}





