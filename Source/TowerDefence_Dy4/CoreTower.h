// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HealthComponentTower.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoreTower.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDamagePlayerSignature, float, DamageTaken);

UCLASS()
class TOWERDEFENCE_DY4_API ACoreTower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACoreTower();

	UPROPERTY(BlueprintAssignable, Category = "Event Dispatcher")
		FDamagePlayerSignature OnDamaged;

	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* collisionBox;

	UFUNCTION()
		void OnBoxHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
		TArray<AActor*> existingHealth;

	UFUNCTION()
		void DestroyTower();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Heath")
		float damageReceived;

};
