// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "Spawner.h"
#include "Engine/World.h"
#include "Waypoint.h"
#include "PlayerPawn.h"
#include "TimerManager.h"
#include "Engine/StaticMesh.h"
#include "EnemyAIController.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	staticMesh->SetupAttachment(RootComponent);

	isBurning = false;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), waypoints);

	corePick = FMath::RandRange(1, 2);
	currentWaypoint = 1;
	maxEnemySpeed = enemySpeed;

	MoveToWaypoints();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (enemySpeed > maxEnemySpeed) enemySpeed = maxEnemySpeed;
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemy::UpdateStats(float increase) // Called in Spawner that has been passed by gameMode
{

	if (increase == NULL)
	{
		increase = 1.0f;
	}

	fullHealth = fullHealth * increase;
	lootDrop = lootDrop * increase;

	UE_LOG(LogTemp, Warning, TEXT("FullHealth: %f"), fullHealth);
	currentHealth = fullHealth;
	currentHealthPercentage = float(currentHealth) / float(fullHealth);
}

void AEnemy::MoveToWaypoints()
{
	AEnemyAiController* EnemyAIController = Cast<AEnemyAiController>(GetController());

	if (EnemyAIController) 
	{ 
		if (currentWaypoint <= waypoints.Num()) 
		{ 
			for (AActor* waypoint : waypoints)
			{
				AWaypoint* waypointIndex = Cast<AWaypoint>(waypoint);

				if (waypointIndex) 
				{
					if (waypointIndex->GetWaypointOrder() == currentWaypoint) 
					{
						//UE_LOG(LogTemp, Warning, TEXT("Core: %d "), corePick);

						//UE_LOG(LogTemp, Warning, TEXT("Waypoint: %d"), currentWaypoint);

						if (corePick == 1)
						{
							EnemyAIController->MoveToActor(waypointIndex, 5.f);
							currentWaypoint++;

							break;
						}
						else
						{
							if (currentWaypoint == 3)
							{
								currentWaypoint = 5;
							}								
							else
							{
								EnemyAIController->MoveToActor(waypointIndex, 5.f);
								currentWaypoint++;

								break;
							}
								
						}
					}
				}
			}
			
		}
	}

}

void AEnemy::Attacked(float damageAmount)
{
	UE_LOG(LogTemp, Warning, TEXT("Damaged"));
	if (damageAmount > 0.0f) {
		currentHealth -= damageAmount;
		currentHealthPercentage = float(currentHealth) / float(fullHealth); 

		if (currentHealth <= 0.0f)
		{
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerPawn::StaticClass(), playerRef);
			APlayerPawn* playerActor = Cast<APlayerPawn>(playerRef[0]);
			playerActor->GainMoney(lootDrop);
			Death();
		}
	}
}

float AEnemy::Attack()
{
	Death();
	return damage;
}

void AEnemy::damageOverTime(int damageAmount, float burnEffect)
{
	//burnEffects = burnEffect;
	
	burnDelegate.BindUFunction(this, FName("BurnEffect"), int(damageAmount), float(burnEffect));

	BurnEffect(damageAmount, burnEffect);

	//UE_LOG(LogTemp, Warning, TEXT("Burn action received "));
}

void AEnemy::BurnEffect(int damaged, float burnDelay)
{
	Attacked(damaged);

	isBurning = true;

	GetWorldTimerManager().SetTimer(burnHandle, burnDelegate, burnDelay, false);
	//UE_LOG(LogTemp, Warning, TEXT("Burn action received: %d "), burnDamage);
}

void AEnemy::Death()
{
	GEngine->AddOnScreenDebugMessage(-1, -5.f, FColor::Blue, TEXT("Enemy Died."));
	Destroy();
}




