// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TurrentBuilder.generated.h"

USTRUCT(BlueprintType)
struct FTurrentConfig
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATurrent> turrentType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int turrentCost;
};

UCLASS()
class TOWERDEFENCE_DY4_API ATurrentBuilder : public AActor
{
	GENERATED_BODY()
	
public:	
	ATurrentBuilder(); // Sets default values for this actor's properties

	UFUNCTION(BlueprintCallable)
		void PassToUiManager(bool isBuilding);

	UFUNCTION(BlueprintCallable)
		void buildTurrent(int turrentNum, int buildChecker);

	UFUNCTION()
		int GetTurrentBuilderOrder();

protected:
	virtual void BeginPlay() override; // Called when the game starts or when spawned
	virtual void Tick(float DeltaTime) override; // Called every frame

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* staticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Turrent")
		TArray<FTurrentConfig> turrent;

private:	
	UPROPERTY()
		TArray<AActor*> uiManager;

	UPROPERTY()
		TArray<AActor*> playerRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Builder", meta = (AllowPrivateAccess = "true"))
		int builderOrder;

};
