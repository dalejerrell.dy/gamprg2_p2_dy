// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

UCLASS()
class TOWERDEFENCE_DY4_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	ABullet();	// Sets default values for this actor's properties

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Conditions")
		float currentSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Conditions")
		float speed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Conditions")
		float burnEffect;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Conditions")
		float explodeDelayTime;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Conditions")
		FTransform currentEnemy;

	UFUNCTION()
		void UpdateStats(bool splash, bool burn, int damage);

protected:
	virtual void BeginPlay() override;	// Called when the game starts or when spawned
	virtual void Tick(float DeltaTime) override;	// Called every frame

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* staticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* collisionSphere;
	UPROPERTY()
		FTimerHandle bulletTimerHandler;

	UFUNCTION()
		virtual void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* otherActor, UPrimitiveComponent* OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:	

	UPROPERTY()
		bool isSplash;
	UPROPERTY()
		bool isBurn;
	UPROPERTY()
		int bulletDamage;
	UPROPERTY()
		TArray<AActor*> enemyRefs;
	UPROPERTY()
		AActor* currEnemy;

	UFUNCTION()
		void MoveBullet(float deltaTime);
	UFUNCTION()
		void BulletType(AActor* otherActor);
	UFUNCTION()
		void Explode();


};
