// Fill out your copyright notice in the Description page of Project Settings.


#include "Turrent.h"
#include "PlayerPawn.h"
#include "Enemy.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Components/ArrowComponent.h"
#include "TimerManager.h"
#include "Bullet.h"

// Sets default values
ATurrent::ATurrent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	staticMesh->SetupAttachment(RootComponent);

	muzzleStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Muzzle"));
	muzzleStaticMesh->SetupAttachment(staticMesh);

	rangeSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Range Sphere"));
	rangeSphere->SetupAttachment(staticMesh);
	rangeSphere->OnComponentBeginOverlap.AddDynamic(this, &ATurrent::OnOverlap);
	rangeSphere->OnComponentEndOverlap.AddDynamic(this, &ATurrent::OnOverlapEnd);

	arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow Component"));

	fireRate = 0.2f;

	giveMoneyDelay = 3.0f;

	enemiesAffected = 0.f;
}

// Called when the game starts or when spawned
void ATurrent::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerPawn::StaticClass(), playerPref);

	if (multiplier >= 100)
	{
		multiplier = 100;
	}

	multiplier = multiplier / 100;
	
	if (isGoldDigger)
	{
		GainGold();
	}

}

// Called every frame
void ATurrent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (currentEnemy != NULL)
	{
		FRotator turrentRot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), currentEnemy->GetActorLocation());
		this->SetActorRotation(turrentRot);
	}
}

void ATurrent::OnOverlap(UPrimitiveComponent * OverlappedComp, AActor * otherActor, UPrimitiveComponent * OtherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Collided"))
	DetermineTargets(otherActor);
}

void ATurrent::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * otherActor, UPrimitiveComponent * OtherComp, int32 otherBodyIndex)
{
	if (AEnemy* enemy = Cast<AEnemy>(otherActor))
	{

		//GetWorldTimerManager().ClearTimer(turrentTimerHandle);
		currentEnemy = NULL;
		enemiesAffected--;

		if (!isHelper)
		{
			if (canShoot)
			{
				targetRefs.Remove(enemy);
				UE_LOG(LogTemp, Warning, TEXT("Enemies remaining: %d"),targetRefs.Num());

				if (targetRefs.Num() > 0)
				{
					DetermineDistanceToEnemy();
				}
				
			}
			else
			{
				enemy->enemySpeed /= multiplier;
			}
		}
	}
	
}

void ATurrent::DetermineTargets(AActor* otherActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("Deteremining Targets"));

	if (!isHelper)
	{
		if (AEnemy* enemy = Cast<AEnemy>(otherActor))
		{
			if (canShoot)
			{
				if (canShootAir)
				{
					targetRefs.Add(enemy);
					DetermineDistanceToEnemy();
				}
				else
				{
					if (enemy->isGround)
					{
						targetRefs.Add(enemy);
						DetermineDistanceToEnemy();
					}
				}
			}
			else
			{
				//EMP Condition
				targetRefs.Add(enemy);
				DetermineTurrentType();
			}

		}

	}
	else
	{
		if (isGoldDigger)
		{
			// Scalper
			if (AEnemy* enemy = Cast<AEnemy>(otherActor))
			{
				targetRefs.Add(enemy);
				DetermineTurrentType();
			}
		}
		else
		{
			// Powerhouse
			if (ATurrent* turrent = Cast<ATurrent>(otherActor))
			{
				targetRefs.Add(turrent);
				DetermineTurrentType();
			}
		}
	}
}

void ATurrent::DetermineDistanceToEnemy()
{
	//UE_LOG(LogTemp, Warning, TEXT("Checking ditance to Targets"));

	TArray<float> distanceArray;
	float distance;

	for (int i = 0; i < targetRefs.Num(); i++)
	{
		if (targetRefs[i] != NULL)
		{
			distance = FVector::Distance(staticMesh->GetComponentLocation(), targetRefs[i]->GetActorLocation());

			distanceArray.Emplace(distance);
		}
	}

	distanceArray.Sort();

	if (targetRefs.Num() > 0)
	{
		currentEnemy = Cast<AEnemy>(targetRefs[0]);

		//UE_LOG(LogTemp, Warning, TEXT("Determening turrent type"));

		DetermineTurrentType();
	}
	else
	{
		currentEnemy = nullptr;
	}
}

void ATurrent::DetermineTurrentType()
{
	if (!isHelper)
	{
		if (canShoot)
		{
			if (canShootAir)
			{
				if (isSplashDamage)
				{
					//Missle Tower Condition
					fireRateDelay();
				}
				else
				{
					//Laser Tower Condition
					fireRateDelay();
				}
			}
			else
			{
				//Basic Turrent Condition
				//UE_LOG(LogTemp, Warning, TEXT("Requested to shoot"));
				fireRateDelay();
			}
		}
		else
		{
			//EMP Condition

			//UE_LOG(LogTemp, Warning, TEXT("EMP in Motion"));
			if (targetRefs[0] != NULL)
			{
				for (int i = 0; i < targetRefs.Num(); i++)
				{
					if (i == enemiesAffected)
					{
						AEnemy* enemy = Cast<AEnemy>(targetRefs[i]);
						enemy->enemySpeed *= multiplier;
						enemiesAffected++;
						//UE_LOG(LogTemp, Warning, TEXT("Enemy Speed = %f"), enemy->enemySpeed);
					}
				}
			}
		}
		
	}
	else
	{
		if (isGoldDigger)
		{
			//Salvager Condition
			//UE_LOG(LogTemp, Warning, TEXT("Should be giving money"));

			APlayerPawn* playerActor = Cast<APlayerPawn>(playerPref[0]);

			for (int i = 0; i < targetRefs.Num(); i++)
			{
				AEnemy* enemy = Cast<AEnemy>(targetRefs[i]);

				if (targetRefs[i] != NULL)
				{
					if (enemy->currentHealth <= 0)
					{
						playerActor->GainMoney(moneyToGive);
					}
				}
			}
		}
		else

		{
			//Power House Condition

			for (int i = 0; i < targetRefs.Num(); i++)
			{
				ATurrent* turrent = Cast<ATurrent>(targetRefs[i]);
				turrent->fireRate /= multiplier;
			}

		}

	}
}

void ATurrent::Shoot()
{
	//UE_LOG(LogTemp, Warning, TEXT("Shot"));

	UWorld* const world = GetWorld();

	if (world)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;

		FVector loc = muzzleStaticMesh->GetComponentLocation();
		FRotator rot = muzzleStaticMesh->GetComponentRotation();

		world->SpawnActor<ABullet>(projectile, loc, rot, SpawnParams)->UpdateStats(isSplashDamage, isBurnEffect, bulletDamage);

		DetermineDistanceToEnemy();

	}
}

void ATurrent::fireRateDelay()
{
	GetWorldTimerManager().SetTimer(turrentTimerHandle, this, &ATurrent::Shoot, fireRate, false);

	//UE_LOG(LogTemp, Warning, TEXT("Implemented delay to shot"));
}

void ATurrent::GainGold()
{
	UE_LOG(LogTemp, Warning, TEXT("Should be giving money"));

	APlayerPawn* playerActor = Cast<APlayerPawn>(playerPref[0]);

	playerActor->GainMoney(moneyToGive);

	GetWorld()->GetTimerManager().SetTimer(turrentTimerHandle, this, &ATurrent::GainGold, giveMoneyDelay, false);

}

void ATurrent::UpdateStats(int turrentCost)
{
	turrentCost = turrentCost / 2;
	cost = turrentCost;
}

void ATurrent::SellTurrent()
{
	APlayerPawn* playerActor = Cast<APlayerPawn>(playerPref[0]);

	playerActor->GainMoney(cost);

	if (!isGoldDigger && isHelper)
	{
		// Power House
		if (targetRefs.Num() != NULL)
		{
			for (int i = 0; i < targetRefs.Num(); i++)
			{
				ATurrent* turrent = Cast<ATurrent>(targetRefs[i]);
				turrent->fireRate *= multiplier;
			}

			Destroy();
		}
		else
		{
			Destroy();
		}
	}
	else
	{
		Destroy();
	}
}

void ATurrent::UpgradeTurrent()
{
	APlayerPawn* playerActor = Cast<APlayerPawn>(playerPref[0]);
	playerActor->turretCost = cost;
	playerActor->SpendTurretMoney();

	rangeSphere->SetSphereRadius(1000.0f);
	fireRate = fireRate / multiplier;
	bulletDamage = bulletDamage / multiplier;
	cost = cost / multiplier;
}



