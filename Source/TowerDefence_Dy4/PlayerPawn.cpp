// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "UiManager.h"
#include "TowerDefence_Dy4GameMode.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	PassToUiManager(currentMoney);
}

bool APlayerPawn::IsBuildAffordable()
{
	return currentMoney >= turretCost;
}

void APlayerPawn::SpendTurretMoney()
{
	currentMoney -= turretCost;
	PassToUiManager(currentMoney);
	//UE_LOG(LogTemp, Warning, TEXT("Current Money: %d"), currentMoney);
}

void APlayerPawn::GainMoney(int killGain) 
{
	//UE_LOG(LogTemp, Warning, TEXT("Current Money: %d"), currentMoney);
	currentMoney += killGain;
	PassToUiManager(currentMoney);
}

void APlayerPawn::PassToUiManager(int data)
{

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AUiManager::StaticClass(), uiManager);

	AUiManager* uiActor = Cast<AUiManager>(uiManager[0]);

	uiActor->coins = data;

}

