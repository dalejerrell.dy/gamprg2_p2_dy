// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefence_Dy4GameMode.generated.h"

USTRUCT(BlueprintType)
struct FWaveConfig
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<TSubclassOf<class AEnemy>> enemyTypes;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float duration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int numToSpawn;

};


UCLASS(minimalapi)
class ATowerDefence_Dy4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ATowerDefence_Dy4GameMode();

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Wave Conditions")
		int maxWave;

	UPROPERTY(EditAnywhere, Category = "Game Conditions")
		bool isDebugging;

protected:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Wave Conditions")
		TArray<FWaveConfig> waveConfig;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Wave Conditions")
		FTimerHandle waveDurationHandle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Wave Conditions")
		float waveDuration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Wave Conditions")
		int currentWaveCycle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemy Conditions")
		int enemyCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wave Conditions")
		int bonusMoney;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Wave Conditions")
		int maxWaveDuration;


private:

	UPROPERTY()
		TArray<AActor*> spawners;

	UPROPERTY()
		TArray<AActor*> remainingEnemies;

	UPROPERTY()
		TArray<AActor*> playerPawn;

	UPROPERTY()
		TArray<AActor*> uiManager;



	UFUNCTION()
		void Starter();
	UFUNCTION()
		void AssignStats();

	UFUNCTION()
		void GiveOutBonusMoney();

	UFUNCTION()
		void WaveCalculations();

	UFUNCTION()
		bool isWaveDone();

	UFUNCTION()
		void PassToUiManager(int lastWave, int currentWave, float waveTime, float maxwaveTime, bool isWin);

};



