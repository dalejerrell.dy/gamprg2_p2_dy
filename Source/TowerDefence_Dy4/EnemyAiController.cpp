// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAiController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Enemy.h"
#include "Engine/World.h"

void AEnemyAiController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) 
{
	AEnemy* enemy = Cast<AEnemy>(GetPawn());

	if (enemy) 
	{
		enemy->MoveToWaypoints();
	}

}
