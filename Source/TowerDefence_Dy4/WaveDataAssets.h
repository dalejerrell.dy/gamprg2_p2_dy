// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveDataAssets.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENCE_DY4_API UWaveDataAssets : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float duration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int numToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<class AEnemy>> enemyTypes;


	
};
