// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class TOWERDEFENCE_DY4_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	// Returns where to spawn inside the box component
	FORCEINLINE class UBoxComponent* GetSpawnBox() const { return spawnBox; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		TArray<TSubclassOf<class AEnemy>> enemyType;

	UPROPERTY(VisibleAnywhere, Category = "Enemy")
		int enemyPicker;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		int numEnemyToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		float enemySpawnDelay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		int enemySpawned;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy")
		float waveIncrease;

	UPROPERTY(VisibleAnywhere, Category = "Spawner")
		FTimerHandle spawnerTimerHandle;

	UFUNCTION()
		void SpawnEnemy();

	UFUNCTION()
		void SpawnDelay();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called on every Frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Spawner")
		class UStaticMeshComponent* staticMesh;

private:

	UPROPERTY(VisibleAnywhere, Category = "Spawner")
	class UBoxComponent* spawnBox;
};
