// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/TimelineComponent.h"
#include "Components/BoxComponent.h"
#include "Enemy.generated.h"

UCLASS()
class TOWERDEFENCE_DY4_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Conditions")
		FString enemyType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Conditions")
		float damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float fullHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float currentHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float currentHealthPercentage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Conditions")
		bool isGround;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Conditions")
		float lootDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Conditions")
		float enemySpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemy Conditions")
		bool isBurning;


	UFUNCTION()
		void UpdateStats(float increase);

	UFUNCTION()
		void MoveToWaypoints();

	UFUNCTION()
		void Attacked(float DamageAmount);

	UFUNCTION(Category = "Attack")
		float Attack();

	UFUNCTION()
		void damageOverTime(int damageAmount, float burnEffect);

	UFUNCTION(Category = "Health")
		void Death();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* staticMesh;




private:
	
	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		int currentWaypoint;

	UPROPERTY(VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		int corePick;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TArray<AActor*> waypoints;

	UPROPERTY(VisibleAnywhere, Category = "Player")
		TArray<AActor*> playerRef;

	UPROPERTY()
		float burnEffects;

	UPROPERTY()
		float maxEnemySpeed;

	UFUNCTION()
		void BurnEffect(int damaged, float burnDelay);

	FTimerDelegate burnDelegate;

	FTimerHandle burnHandle;
};
